{
  description = "My personal neovim configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    neovim = {
      url = "github:neovim/neovim?dir=contrib";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nvim-treesitter = { url = "github:nvim-treesitter/nvim-treesitter"; flake = false;};
  };

  outputs = { self, nixpkgs, flake-utils, neovim, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            neovim.overlay
          ];
        };

        my-neovim = pkgs.wrapNeovim pkgs.neovim {
          viAlias = true;
          vimAlias = true;
          configure = {
          customRC = "set number";

          packages.myVimPackage = with pkgs.vimPlugins; {
            start = [
              fugitive
            ];
            opt = [];
          };
        };
        };
      in
      {
        defaultPackage = my-neovim;
      });
}
